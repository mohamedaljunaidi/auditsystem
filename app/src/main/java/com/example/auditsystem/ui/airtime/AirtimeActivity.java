package com.example.auditsystem.ui.airtime;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.app.Dialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.auditsystem.R;

public class AirtimeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.airtime_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.recharge) {
            recharge();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void dialogOptions(Dialog d, View v){
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(v);
        d.show();
        Window window_register = d.getWindow();
        window_register.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.MATCH_PARENT);
    }


    public void recharge(){
        LayoutInflater inflater= getLayoutInflater();
        View v= inflater.inflate(R.layout.airtime_recharge_dialog,null);

        Spinner spOperator = v.findViewById(R.id.sp_operator);
        Spinner spCardValues= v.findViewById(R.id.sp_card_values);
        EditText etNumberCode = v.findViewById(R.id.et_recharge_code);
        EditText etFullnumber = v.findViewById(R.id.et_fullnumber);
        Button btnAdd = v.findViewById(R.id.btn_add);

        spOperator.setAdapter(getArrayAdapter(R.array.country));
        spCardValues.setAdapter(getArrayAdapter(R.array.location));

        btnAdd.setOnClickListener(v1 -> {
            String operator = spOperator.getSelectedItem().toString();
            String cardValue = spCardValues.getSelectedItem().toString();
            String numbercode = etNumberCode.getText().toString();
            String fullnumber = etFullnumber.getText().toString();
        });


        final Dialog dialog = new Dialog(this);

        dialogOptions(dialog,v);
    }

    private ArrayAdapter<String> getArrayAdapter(int array){
        ArrayAdapter<String> myAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1, getResources().getStringArray(array));
        myAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        return myAdapter;
    }
}
