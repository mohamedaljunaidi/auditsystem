package com.example.auditsystem.ui.payment_accounts.fragments;


import android.app.Dialog;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.evrencoskun.tableview.TableView;
import com.evrencoskun.tableview.adapter.AbstractTableAdapter;
import com.evrencoskun.tableview.filter.Filter;
import com.evrencoskun.tableview.pagination.Pagination;
import com.example.auditsystem.R;
import com.example.auditsystem.tableview.TableViewAdapter;
import com.example.auditsystem.tableview.TableViewListener;
import com.example.auditsystem.tableview.TableViewModel;
import com.google.android.material.textfield.TextInputEditText;
import com.ybs.countrypicker.CountryPicker;
import com.ybs.countrypicker.CountryPickerListener;

import org.w3c.dom.Text;


public class WestrenUnionFragment extends Fragment {


    private static final String ARG_SECTION_NUMBER = "section_number";
    private EditText searchField;
    private Spinner moodFilter, genderFilter, itemsPerPage;
    public ImageButton previousButton, nextButton;
    public EditText pageNumberField;
    public TextView tablePaginationDetails;

    private AbstractTableAdapter mTableViewAdapter;
    private TableView mTableView;
    private Filter mTableFilter; // This is used for filtering the table.
    private Pagination mPagination; // This is used for paginating the table.

    private TableViewModel mTableViewModel;

    private boolean mPaginationEnabled = false;


    @BindView(R.id.countryName)
    TextView tvCountryName;

    @BindView(R.id.txt_country_code)
    TextView tvCountryCode;

    @BindView(R.id.et_first_name)
    TextInputEditText etFirstname;

    @BindView(R.id.et_middle_name)
    TextInputEditText etMiddlename;

    @BindView(R.id.et_last_name)
    TextInputEditText etLastname;

    @BindView(R.id.et_city)
    TextInputEditText etCity;

    @BindView(R.id.et_mobile_number)
    TextInputEditText etMobile;

    @BindView(R.id.btn_add)
    Button btnAdd;

    public WestrenUnionFragment() {

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v= inflater.inflate(R.layout.fragment_westren_union, container, false);
        init(v);
        Button dialog=v.findViewById(R.id.btn_add_western_union);

        dialog.setOnClickListener(v1 -> requestPaymentDialog());
        return v;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_payment_accounts, menu);
        MenuItem item=menu.findItem(R.id.bank);
        item.setVisible(false);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.western:
                requestPaymentDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void dialogOptions(Dialog d, View v){
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(v);
        d.show();
        Window window_register = d.getWindow();
        window_register.setLayout(ViewPager.LayoutParams.MATCH_PARENT, ViewPager.LayoutParams.WRAP_CONTENT);
    }


    public void requestPaymentDialog(){
        LayoutInflater inflater= getLayoutInflater();
        View v= inflater.inflate(R.layout.western_union_dialog,null);
        ButterKnife.bind(this,v);
        TextView txtCountry = v.findViewById(R.id.countryName);
        TextView txtCountryCode = v.findViewById(R.id.txt_country_code);

        txtCountry.setOnClickListener(v12 -> countryPicker(txtCountry,1));
        txtCountryCode.setOnClickListener(v12 -> countryPicker(txtCountryCode,0));


        btnAdd.setOnClickListener(v1 -> {
            String firstName = etFirstname.getText().toString();
            String middleName = etMiddlename.getText().toString();
            String lastName = etLastname.getText().toString();
            String country = txtCountry.getText().toString();
            String city = etCity.getText().toString();
            String mobile = txtCountryCode.getText().toString()+etMobile.getText().toString();
            Toast.makeText(getActivity(), "ok", Toast.LENGTH_SHORT).show();
        });

        final Dialog dialog = new Dialog(getActivity());

        dialogOptions(dialog,v);

    }

    private void countryPicker(TextView tvText, int flag) {
        CountryPicker picker = CountryPicker.newInstance("Select Country Code");  // dialog title
        picker.setListener((name, code, dialCode, flagDrawableResID) -> {
            if (flag == 0) {
                tvText.setText(dialCode);
            } else {
                tvText.setText(name);
            }
            picker.dismiss();
        });
        picker.show(getActivity().getSupportFragmentManager(), "COUNTRY_CODE_PICKER");
    }


    private ArrayAdapter<String> getArrayAdapter(int array){
        ArrayAdapter<String> myAdapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1, getResources().getStringArray(array));
        myAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        return myAdapter;
    }


    public void init(View v){
        searchField = v.findViewById(R.id.query_string);
        searchField.addTextChangedListener(mSearchTextWatcher);

        moodFilter = v.findViewById(R.id.mood_spinner);
        moodFilter.setOnItemSelectedListener(mItemSelectionListener);

        genderFilter = v.findViewById(R.id.gender_spinner);
        genderFilter.setOnItemSelectedListener(mItemSelectionListener);

        itemsPerPage = v.findViewById(R.id.items_per_page_spinner);

        View tableTestContainer = v.findViewById(R.id.table_test_container);

        previousButton = v.findViewById(R.id.previous_button);
        nextButton = v.findViewById(R.id.next_button);
        pageNumberField =v.findViewById(R.id.page_number_text);
        tablePaginationDetails = v.findViewById(R.id.table_details);

        if (mPaginationEnabled) {
            tableTestContainer.setVisibility(View.VISIBLE);
            itemsPerPage.setOnItemSelectedListener(onItemsPerPageSelectedListener);

            previousButton.setOnClickListener(mClickListener);
            nextButton.setOnClickListener(mClickListener);
            pageNumberField.addTextChangedListener(onPageTextChanged);
        } else {
            tableTestContainer.setVisibility(View.GONE);
        }

        // Let's get TableView
        mTableView = v.findViewById(R.id.tableview);

        initializeTableView();

        if (mPaginationEnabled) {
            mTableFilter = new Filter(mTableView); // Create an instance of a Filter and pass the
            // created TableView.

            // Create an instance for the TableView pagination and pass the created TableView.
            mPagination = new Pagination(mTableView);

            // Sets the pagination listener of the TableView pagination to handle
            // pagination actions. See onTableViewPageTurnedListener variable declaration below.
            mPagination.setOnTableViewPageTurnedListener(onTableViewPageTurnedListener);
        }

    }



    private void initializeTableView() {
        // Create TableView View model class  to group view models of TableView
        mTableViewModel = new TableViewModel(getActivity());

        // Create TableView Adapter
        mTableViewAdapter = new TableViewAdapter(getActivity(), mTableViewModel);

        mTableView.setAdapter(mTableViewAdapter);
        mTableView.setTableViewListener(new TableViewListener(mTableView));

        // Create an instance of a Filter and pass the TableView.
        //mTableFilter = new Filter(mTableView);

//        ArrayList<ColumnHeader> header = new ArrayList<>();
//        ColumnHeader columnHeader = new ColumnHeader("id","data");
//        header.add(columnHeader);
//
//
//        ArrayList<RowHeader> row = new ArrayList<>();
//        RowHeader rowHeader = new RowHeader("id","data");
//        row.add(rowHeader);



        // Load the dummy data to the TableView
        mTableViewAdapter.setAllItems(mTableViewModel.getColumnHeaderList(), mTableViewModel.getRowHeaderList(), mTableViewModel.getCellList());


        //mTableView.setHasFixedWidth(true);

        /*for (int i = 0; i < mTableViewModel.getCellList().size(); i++) {
            mTableView.setColumnWidth(i, 200);
        }*)

        //mTableView.setColumnWidth(0, -2);
        //mTableView.setColumnWidth(1, -2);

        /*mTableView.setColumnWidth(2, 200);
        mTableView.setColumnWidth(3, 300);
        mTableView.setColumnWidth(4, 400);
        mTableView.setColumnWidth(5, 500);*/

    }

    public void filterTable(String filter) {
        // Sets a filter to the table, this will filter ALL the columns.
        //mTableFilter.set(filter);
    }

    public void filterTableForMood(String filter) {
        // Sets a filter to the table, this will only filter a specific column.
        // In the example data, this will filter the mood column.
        mTableFilter.set(TableViewModel.MOOD_COLUMN_INDEX, filter);
    }

    public void filterTableForGender(String filter) {
        // Sets a filter to the table, this will only filter a specific column.
        // In the example data, this will filter the gender column.
        mTableFilter.set(TableViewModel.GENDER_COLUMN_INDEX, filter);
    }

    // The following four methods below: nextTablePage(), previousTablePage(),
    // goToTablePage(int page) and setTableItemsPerPage(int itemsPerPage)
    // are for controlling the TableView pagination.
    public void nextTablePage() {
        mPagination.nextPage();
    }

    public void previousTablePage() {
        mPagination.previousPage();
    }

    public void goToTablePage(int page) {
        mPagination.goToPage(page);
    }

    public void setTableItemsPerPage(int itemsPerPage) {
        mPagination.setItemsPerPage(itemsPerPage);
    }

    // Handler for the changing of pages in the paginated TableView.
    private Pagination.OnTableViewPageTurnedListener onTableViewPageTurnedListener = new
            Pagination.OnTableViewPageTurnedListener() {
                @Override
                public void onPageTurned(int numItems, int itemsStart, int itemsEnd) {
                    int currentPage = mPagination.getCurrentPage();
                    int pageCount = mPagination.getPageCount();
                    previousButton.setVisibility(View.VISIBLE);
                    nextButton.setVisibility(View.VISIBLE);

                    if (currentPage == 1 && pageCount == 1) {
                        previousButton.setVisibility(View.INVISIBLE);
                        nextButton.setVisibility(View.INVISIBLE);
                    }

                    if (currentPage == 1) {
                        previousButton.setVisibility(View.INVISIBLE);
                    }

                    if (currentPage == pageCount) {
                        nextButton.setVisibility(View.INVISIBLE);
                    }

                    tablePaginationDetails.setText(getString(R.string.table_pagination_details, String
                            .valueOf(currentPage), String.valueOf(itemsStart), String.valueOf(itemsEnd)));

                }
            };


    private AdapterView.OnItemSelectedListener mItemSelectionListener = new AdapterView
            .OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            // 0. index is for empty item of spinner.
            if (position > 0) {

                String filter = Integer.toString(position);

                if (parent == moodFilter) {
                    filterTableForMood(filter);
                } else if (parent == genderFilter) {
                    filterTableForGender(filter);
                }
            }
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
            // Left empty intentionally.
        }
    };

    private TextWatcher mSearchTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            filterTable(String.valueOf(s));
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


    private AdapterView.OnItemSelectedListener onItemsPerPageSelectedListener = new AdapterView
            .OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            int itemsPerPage;
            switch (parent.getItemAtPosition(position).toString()) {
                case "All":
                    itemsPerPage = 0;
                    break;
                default:
                    itemsPerPage = Integer.valueOf(parent.getItemAtPosition(position).toString());
            }

            setTableItemsPerPage(itemsPerPage);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    private View.OnClickListener mClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == previousButton) {
                previousTablePage();
            } else if (v == nextButton) {
                nextTablePage();
            }
        }
    };

    private TextWatcher onPageTextChanged = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            int page;
            if (TextUtils.isEmpty(s)) {
                page = 1;
            } else {
                page = Integer.valueOf(String.valueOf(s));
            }

            goToTablePage(page);
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };


}
