package com.example.auditsystem.ui;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.example.auditsystem.MainActivity;
import com.example.auditsystem.R;
import com.example.auditsystem.util.Util;
import com.google.android.material.textfield.TextInputEditText;

public class LoginActivity extends AppCompatActivity {


    @BindView(R.id.et_username)
    TextInputEditText etUsername;

    @BindView(R.id.et_password)
    TextInputEditText etPassword;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login2);
        ButterKnife.bind(this);
    }


    @OnClick(R.id.btnActivityLoginLogin)
    public void login(){
        if (etUsername.getText().length() > 0 && etPassword.getText().length() > 0) {
            Toasty.success(this, "Success Login", Toast.LENGTH_SHORT, true).show();
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else {
            Toasty.error(this, "Username or Password is Empty", Toast.LENGTH_SHORT, true).show();
        }
    }
}
