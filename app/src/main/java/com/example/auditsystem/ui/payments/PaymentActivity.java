package com.example.auditsystem.ui.payments;

import com.google.android.material.tabs.TabLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.auditsystem.R;
import com.example.auditsystem.ui.payments.fragments.CompletedPaymentsFragment;
import com.example.auditsystem.ui.payments.fragments.ProgressPaymentFragment;
import com.example.auditsystem.ui.payments.fragments.RejectedPaymentsFragment;
import com.example.auditsystem.ui.payments.fragments.RequestFragment;
import com.example.auditsystem.ui.payments.fragments.SuccessfulPaymentsFragment;
import com.example.auditsystem.util.NonSwipeableViewPager;

public class PaymentActivity extends AppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.container)
    NonSwipeableViewPager mViewPager;

    private SectionsPagerAdapter mSectionsPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.container);
        mViewPager.setOffscreenPageLimit(0);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);

        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager));


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_payment2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
//                    RequestFragment request = new RequestFragment();
//                    return request;

                    ProgressPaymentFragment progressPaymentFragment = new ProgressPaymentFragment();
                    return progressPaymentFragment;
                case 1:
                    SuccessfulPaymentsFragment successfulPaymentsFragment = new SuccessfulPaymentsFragment();
                    return successfulPaymentsFragment;
                case 2:
                    RejectedPaymentsFragment rejectedPaymentsFragment = new RejectedPaymentsFragment();
                    return rejectedPaymentsFragment;
                case 3:
                    CompletedPaymentsFragment completedPaymentsFragment = new CompletedPaymentsFragment();
                    return completedPaymentsFragment;

                default:
//                    RequestFragment requestFragment = new RequestFragment();
//                    return requestFragment;
                ProgressPaymentFragment progressPaymentFragment2 = new ProgressPaymentFragment();
                return progressPaymentFragment2;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}
