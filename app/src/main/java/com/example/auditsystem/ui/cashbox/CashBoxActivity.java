package com.example.auditsystem.ui.cashbox;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.animation.ValueAnimator;
import android.os.Bundle;
import android.widget.Button;
import android.widget.TextView;

import com.example.auditsystem.R;

import java.text.DecimalFormat;

public class CashBoxActivity extends AppCompatActivity {


    @BindView(R.id.tv_stock_balance)
    TextView tvStockBalance;

    @BindView(R.id.et_available_cash)
    TextView tvAvailableCash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_box);
        ButterKnife.bind(this);
        startCountAnimation(tvStockBalance,600);
        startCountAnimation(tvAvailableCash, 3899750);
    }


    private void startCountAnimation(TextView tvValue, int value) {
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(1000);
        animator.addUpdateListener(animation -> tvValue.setText(animation.getAnimatedValue().toString()));
        animator.start();
    }
}
