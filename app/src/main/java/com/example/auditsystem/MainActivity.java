package com.example.auditsystem;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.example.auditsystem.ui.airtime.AirtimeActivity;
import com.example.auditsystem.ui.calculator.CalculatorActivity;
import com.example.auditsystem.ui.cashbox.CashBoxActivity;
import com.example.auditsystem.ui.payment_accounts.PaymentAccountsActivity;
import com.example.auditsystem.ui.purchases2.PurchasesActivity;
import com.example.auditsystem.ui.sim_cards.SimCardActivity;
import com.example.auditsystem.ui.slots.SlotActivity;
import com.google.android.material.navigation.NavigationView;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.view.Menu;
import android.view.MenuItem;
<<<<<<< HEAD
import android.widget.Toast;

import com.example.auditsystem.ui.payments.PaymentActivity;
=======
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.auditsystem.ui.payments.PaymentActivity;
import com.example.auditsystem.ui.purchases.PurchasesActivity;
import com.hanks.htextview.base.AnimationListener;
import com.hanks.htextview.base.HTextView;
>>>>>>> bafe54a73b40fb0d08627f461c35704c58b57b84
import com.hanks.htextview.line.LineTextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.tv_title)
    TextView tvTitle;


    @BindView(R.id.count1)
    TextView count1;

    @BindView(R.id.count2)
    TextView count2;

    @BindView(R.id.count3)
    TextView count3;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        startCountAnimation(count1,21);
        startCountAnimation(count2,500);
        startCountAnimation(count3,250);

    }



    private void startCountAnimation(TextView tvValue, int value) {
        ValueAnimator animator = ValueAnimator.ofInt(0, value);
        animator.setDuration(2000);
        animator.addUpdateListener(animation -> tvValue.setText(animation.getAnimatedValue().toString()));
        animator.start();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_Payments) {
            Intent intent = new Intent(this, PaymentActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_Purchases) {
            Intent myIntent = new Intent(this, PurchasesActivity.class);
            startActivity(myIntent);
        } else if (id == R.id.nav_AIR_TIME) {
            Intent i = new Intent(this, AirtimeActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_SIM_Cards) {
            Intent i = new Intent(this, SimCardActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_SLOTS) {
            Intent i = new Intent(this, SlotActivity.class);
            startActivity(i);
        } else if (id == R.id.Nav_Cash_Boxs) {
            Intent i = new Intent(this, CashBoxActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_Calculator) {
            Intent i = new Intent(this, CalculatorActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_Payment_Details) {
            Intent i = new Intent(this, PaymentAccountsActivity.class);
            startActivity(i);
        } else if (id == R.id.nav_Logout) {
//            Intent i = new Intent(this, PaymentAccountsActivity.class);
//            startActivity(i);
            Toast.makeText(this, "Logged out", Toast.LENGTH_SHORT).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @OnClick(R.id.btn_payments)
    public void payments() {
        Intent intent = new Intent(this, PaymentActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_sim_card)
    public void sim_card() {
        Intent intent = new Intent(this, PurchasesActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_airtime)
    public void airtime() {
        Intent intent = new Intent(this, AirtimeActivity.class);
        startActivity(intent);
    }

//    @OnClick(R.id.btn_purchases)
//    public void purchases() {
//        Intent intent = new Intent(this, AirtimeActivity.class);
//        startActivity(intent);
//    }


    @OnClick(R.id.btn_slots)
    public void slots() {
        Intent intent = new Intent(this, SlotActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_cash_box)
    public void cashBox() {
        Intent intent = new Intent(this, CashBoxActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_calculator)
    public void calculator() {
        Intent intent = new Intent(this, CalculatorActivity.class);
        startActivity(intent);
    }


    @OnClick(R.id.btn_payment_accounts)
    public void payment_accounts() {
        Intent intent = new Intent(this, PaymentAccountsActivity.class);
        startActivity(intent);
    }


}
